#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>

int main(void)
{
	int sd;
	struct sockaddr_in server_addr;
	struct hostent *hp;
	int num1, num2, num_leido, res;
	char mensaje[40];

	sd = socket(AF_INET, SOCK_STREAM, 0);
	if (sd < 0){
		printf("Error en socket\n");
		return 1;
	}

	/* se obtiene y rellena la dirección del servidor */
	hp = gethostbyname ("127.0.0.1");
	if (hp == NULL){
		printf("Error en la llamada gethostbyname\n");
		return 1;
	}
	memcpy (&(server_addr.sin_addr), hp->h_addr, hp->h_length);

	server_addr.sin_family = AF_INET;
	// server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	server_addr.sin_port = htons(4200);

	/* se establece la conexión */
	if (connect(sd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
		printf("Error en la llamada connect");
		return 1;
	}
	// Se lee el mensaje del servidor
	read(sd, mensaje, sizeof(mensaje));
	printf ("Mensaje recibido: %s\n", mensaje);
	scanf ("%d", &num_leido);
	num1=htonl(num_leido);
	
	/* los argumentos se convierten a formato de red */
	write(sd, (char *)&num1, sizeof(int));
	
	// Se lee el mensaje del servidor
	read(sd, mensaje, sizeof(mensaje));
	printf ("Mensaje recibido: %s\n", mensaje);
	scanf ("%d", &num_leido);
	num2=htonl(num_leido);

	/* los argumentos se convierten a formato de red */
	write(sd, (char *)&num2, sizeof(int));

	/* envía la petición */
	read(sd, (char *)&res, sizeof(int));
	
	/* recibe la respuesta */
	res = ntohl(res); /* se convierte el resultado al formato del computador */
	printf("Resultado es %d \n", res);
	close (sd);
	return 0;
}


