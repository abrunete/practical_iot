#include <stdio.h>
#include <sys/time.h> 
#include <sys/types.h>    
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

int main(void)
{
  struct sockaddr_in server_addr,client_addr;
  int sd, sc;
  int size;
  int num[2], res; 

  sd = socket(AF_INET, SOCK_STREAM, 0);
  if (sd < 0) {
    printf("Error en la llamada socket\n");
    return 1;
  }

  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = INADDR_ANY; // inet_addr("127.0.0.1");
  server_addr.sin_port = htons(4200);

  int on=1;
  setsockopt(sd,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));

  if (bind(sd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0){
    printf("Error en la llamada bind\n");
    return 1;
  }
  listen(sd, 5);
  size = sizeof(client_addr);
  while (1){
    printf("Esperando conexion...\n");
    sc = accept(sd, (struct sockaddr *)&client_addr, &size);
    if (sc < 0){
      printf("Error en accpet\n");
      break;
    }
    /* recibe la petición, dos números enteros */
    if (read(sc, (char *) num, 2 *sizeof(int)) < 0){
      printf("Error en read\n");
      break;
    }
    /* los datos se transforman del formato de red al del computador */
    printf("Sumando: %d + %d\n", ntohl(num[0]), ntohl(num[1])); 
    res = ntohl(num[0]) + ntohl(num[1]); /* obtiene el resultado */
    printf("Enviando resultado: %d\n", res);
    res = htonl(res);	/* el resultado se transforma a formato de red */
    /* envía el resultado y cierra la conexión */
    if (write(sc, (char *)&res, sizeof(int)) < 0){
      printf("Error en write\n");
      break;
    }
    close(sc);
  }
  close (sd);
  return 0;
}


