/*
  Codigo de la maqueta de practicas de Computadores y Programacion
  @author Raquel Cedazo
*/

// Enciende y apaga un LED conectado al pin digital 12
// El LED parpadeara el numero de veces indicado por un numero ASCII leido del puerto serie 
int led = 12;
String cadena;
float tempC;  // temperatura en grados Celsius
int reading;

void setup() {                
  pinMode(led, OUTPUT);   // se declara el pin digital del LED como de salida
  Serial.begin(9600);    // se conecta al puerto serie
}

void loop() {
  if (Serial.available()) {
    cadena = Serial.readString();      // se lee del puerto serie

    // Si es una operacion de escritura
    if (cadena[0] == 'w') {
      // Si el segundo caracter es l, se enciende el sensor LED tantas veces 
      // como indique el valor del tercer caracter
      if (cadena[1] == 'l') {
        int valor = cadena[2] - '0';
        if (valor > 0 && valor < 9) {
          for (int i=0; i<valor; i++) {
            digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
            delay(150);               // wait for a second
            digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
            delay(150);               // wait for a second
          }
        }
      }
    }
    // Si es una operacion de lectura
    else if (cadena[0] == 'r') {
      // Lee la temperatura del sensor LM35
      if (cadena[1] == 't') {
        reading = analogRead(1); // pin analogico A1
        tempC = (5.0 * reading * 100.0) / 1024;
        Serial.print(tempC);
      }
    }
  }
}
