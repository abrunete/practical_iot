IF_MATCH=False

schema = {
    # Schema definition, based on Cerberus grammar. Check the Cerberus project
    # (https://github.com/nicolaiarocci/cerberus) for details.
    'ciudad': {
        'type': 'string',
        'minlength': 1,
        'maxlength': 10,
    },
    'codigopostal': {
        'type': 'string',
        'minlength': 1,
        'maxlength': 6,
        'required': True,
        # talk about hard constraints! For the purpose of the demo
        # 'lastname' is an API entry-point, so we need it to be unique.
        #'unique': True,
    },
    # 'tipo' is a list, and can only contain values from 'allowed'.
    'tipo': {
        'type': 'string',
        'allowed': ["temp", "hum", "ilu"],
    },
    'fecha': {
        'type': 'datetime',
    },
    'valor': {
        'type': 'float',
        'required': True,
    }
}

medida = {
    # 'title' tag used in item links. Defaults to the resource title minus
    # the final, plural 's' (works fine in most cases but not for 'people')
    'item_title': 'medida',

    # by default the standard item entry point is defined as
    # '/people/<ObjectId>'. We leave it untouched, and we also enable an
    # additional read-only entry point. This way consumers can also perform
    # GET requests at '/people/<lastname>'.
    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'codigopostal'
    },

    # We choose to override global cache-control directives for this resource.
    'cache_control': 'max-age=10,must-revalidate',

    # We choose to override global cache-control directives for this resource.
    'cache_control': 'max-age=10,must-revalidate',
    'cache_expires': 10,

    # most global settings can be overridden at resource level
    'resource_methods': ['GET', 'POST', 'DELETE'],
    'item_methods': ['GET', 'PATCH', 'PUT', 'DELETE'],

    'schema': schema
}

DOMAIN = {
    'medida': medida,
}