#!/usr/bin/python3
import http.client
import urllib.request
import urllib.parse
import json
import sys
import time

# We read the URL from the command line, for example: python3 ./demo-get.py http://127.0.0.1:5000/medida
api_url = sys.argv[1]

#timestamp = int(time.time())
#params = {"order": -1}
#binary_data = json.dumps(params).encode('ascii')
'''header = {"User-Agent": "raspberrycarriots",
              "Content-Type": "application/json",
              "carriots.apikey": api_key}'''
req = urllib.request.Request(api_url)
#req.get_method = lambda: "DELETE"
f = urllib.request.urlopen(req)

#print(f.read().decode('utf-8'))

#data=json.loads(f.read().decode('utf-8'))
data=json.loads(f.read().decode('utf-8'))
print(json.dumps(data,indent=4,sort_keys=True,ensure_ascii=False))

    
