#!/usr/bin/python3
import http.client
import urllib.request
import urllib.parse
import json


base_url = "http://127.0.0.1:5000/medida/"
id_val = "5a18163074fece08e0846775"
api_url = base_url + id_val

params = {"valor": 111}

binary_data = json.dumps(params).encode('ascii')

req = urllib.request.Request(api_url,binary_data)

# To patch an item, it is necessary to include a header with the _etag
# Simply calling a PATCH request won't patch the item. In order to patch an item, 
# we also need to provide an _etag related to a particular item. Once item id and _etag match, 
# the item is deleted from the database.

req.add_header('If-Match','f533cbfd09c931a0b86883b12d34c1d8136eb3e0')
req.add_header('Content-type','application/json; charset=utf-8')

req.get_method = lambda: "PATCH"
f = urllib.request.urlopen(req)
print(f.read().decode('utf-8'))



    
