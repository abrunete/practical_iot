#!/usr/bin/python3
import http.client
import urllib.request
import urllib.parse
import json


api_url = "http://127.0.0.1:5000/medida/5a18163074fece08e0846775"

req = urllib.request.Request(api_url)

# To delete an item, it is necessary to include a headet with the _etag
# Simply calling a DELETE request won't delete the item. In order to delete an item, 
# we also need to provide an _etag related to a particular item. Once item id and _etag match, 
# the item is deleted from the database.
req.add_header('If-Match','de2a5c22d5dfbadf8a9210b0458f4666307065f3')

req.get_method = lambda: "DELETE"
f = urllib.request.urlopen(req)
print(f.read().decode('utf-8'))



    
