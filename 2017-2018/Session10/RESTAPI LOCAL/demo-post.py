#!/usr/bin/python3
import http.client
import urllib.request
import urllib.parse
import json

import time
import datetime


api_url = "http://127.0.0.1:5000/medida/"

headers = {'Content-type': 'application/json'}

params = {"identificador": "abg5",
  "ciudad": "berlin",
  "fecha": str(datetime.datetime.now()),
  "valor": 24.95}

binary_data = json.dumps(params).encode('ascii')

req = urllib.request.Request(api_url,binary_data,headers)

req.get_method = lambda: "POST"
f = urllib.request.urlopen(req)

#print(f.read().decode('utf-8'))
data=json.loads(f.read().decode('utf-8'))
print(json.dumps(data,indent=4,sort_keys=True))
