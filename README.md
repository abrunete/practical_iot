# README #

This is the official repository for the course "Practical IoT with Raspberry Pi"

### What is this repository for? ###

* Here you will find the course material: program files, demos, and tutorial.

### How do I get set up? ###

You will need:

* Raspberry Pi
* Python environment
* Sensors and Actuators to play (or the demo board provided by UPM)

### Who do I talk to? ###

* alberto.brunete (at) upm.es
* raquel.cedazo (at) upm.es