# Cliente que pide dos numeros por teclado y se los envia 
# al servidor, quien le devuelve el resultado de la suma 
#!/usr/bin/python3
import socket
import sys

def client():
        # Check number of parameters
        if len (sys.argv) != 3:
                print('Usage: python3 client.py IP PORT')
                sys.exit(1)

        host = sys.argv[1]
        port = int(sys.argv[2])
         
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.connect((host,port))
         
        message1 = input("First num: ")
        mySocket.send(message1.encode())
        message2 = input("Second num: ")
        mySocket.send(message2.encode())
        
        data = mySocket.recv(1024).decode()
        print ('Received from server: ' + data)
        mySocket.close()
 
client()
