#!/usr/bin/python3
import http.client
import urllib.request
import urllib.parse
import json

import time


api_url = "http://api.carriots.com/streams"
device = "Pi1@fishminator.fishminator"  # Replace with the id_developer of your device
api_key = "5f62b66dfbe2ae01628642018618e4397c6769c5a06171d5d2c47969e08b6bb7"  # Replace w
content_type = "application/json"     


timestamp = int(time.time())

mydata = {'tipo': 'temp',
      'identificador': 't003',
      'ciudad': 'Segovia',
      'fecha': 'hoy',
      'valor': 30}

params = {"protocol": "v2",
              "device": device,
              "at": timestamp,
              "data": mydata}
binary_data = json.dumps(params).encode('ascii')

header = {"User-Agent": "raspberrycarriots", "Content-Type": content_type,"carriots.apikey": api_key}

req = urllib.request.Request(api_url,binary_data,header)
f = urllib.request.urlopen(req)
print(f.read().decode('utf-8'))



    
