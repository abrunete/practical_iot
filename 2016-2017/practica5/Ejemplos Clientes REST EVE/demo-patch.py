#!/usr/bin/python3
import http.client
import urllib.request
import urllib.parse
import json


api_url = "http://127.0.0.1:5000/medida/5910a757a67fff063bc8d6ff"


params = {"valor": 13}
binary_data = json.dumps(params).encode('ascii')

req = urllib.request.Request(api_url,binary_data)

# To delete an item, it is necessary to include a headet with the _etag
# Simply calling a DELETE request won't delete the item. In order to delete an item, 
# we also need to provide an _etag related to a particular item. Once item id and _etag match, 
# the item is deleted from the database.
req.add_header('If-Match','64761e90aca2c752a7a2aba550f089819842929b')
req.add_header('Content-type','application/json; charset=utf-8')

req.get_method = lambda: "PATCH"
f = urllib.request.urlopen(req)
print(f.read().decode('utf-8'))



    
