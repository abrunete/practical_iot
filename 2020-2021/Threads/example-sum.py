#!/usr/bin/python3

# Program that calculates the sum of the 1000 first numbers 
# with 10 threads working in parallel

import threading
import time

exitFlag = 0
sumTotal = 0

class myThread (threading.Thread):
   def __init__(self, threadID, name, index):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.index = index
   def run(self):
      print ("Starting " + self.name)
      partial_sum(self.name, self.index)
      print ("Exiting " + self.name)

def partial_sum(threadName, index):
      if exitFlag:
         threadName.exit()
      beginning = index*10 + 1;
      end = beginning + 10;
      sumPartial = 0
      for i in range(beginning, end):
         sumPartial += i
      global sumTotal
      threadLock.acquire() # Acquire Lock for shared variable sumTotal
      sumTotal += sumPartial
      threadLock.release() # Release Lock
      print("Sum partial-"+str(index)+" : "+str(sumPartial))
      
threads = [] # Thread list
threadLock = threading.Lock() # Synchronizing

# Create threads
for i in range(10):
   newThread = myThread(i, "Thread-Sum-"+str(i), i)
   newThread.start()
   threads.append(newThread)
   
# Wait for all threads to complete
for t in threads:
	t.join()

# Print the result
print ("Sum Total:" + str(sumTotal))

print ("Exiting Main Thread")
