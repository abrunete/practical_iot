#!/usr/bin/python3
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

PIN_LED1=17
PIN_LED2=27
PIN_LED3=22

def all_leds_on():
	GPIO.setup(PIN_LED1,GPIO.OUT)
	GPIO.setup(PIN_LED2,GPIO.OUT)
	GPIO.setup(PIN_LED3,GPIO.OUT)
	GPIO.output(PIN_LED1, True)
	GPIO.output(PIN_LED2, True)
	GPIO.output(PIN_LED3, True)

def all_leds_off():
	GPIO.setup(PIN_LED1,GPIO.OUT)
	GPIO.setup(PIN_LED2,GPIO.OUT)
	GPIO.setup(PIN_LED3,GPIO.OUT)
	GPIO.output(PIN_LED1, False)
	GPIO.output(PIN_LED2, False)
	GPIO.output(PIN_LED3, False)

def switch_on_led(pin):
	GPIO.setup(pin,GPIO.OUT)
	GPIO.output(pin, True)

def switch_off_led(pin):
	GPIO.setup(pin,GPIO.OUT)
	GPIO.output(pin, False)

# TEST:

# all_leds_on()
# time.sleep(3)
# all_leds_off()

# time.sleep(2)

# switch_on_led(PIN_LED1)
# time.sleep(3)
# switch_off_led(PIN_LED1)
