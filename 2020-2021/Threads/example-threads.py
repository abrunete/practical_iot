#!/usr/bin/python3

import threading
import time
# global variables
exitFlag = 0
temperature = 0

class myThread (threading.Thread):
    def __init__(self, threadID, name, delay):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.delay = delay
    def run(self):
        print ("Starting " + self.name)
        print_time(self.name, self.delay)
        print ("Exiting " + self.name)

def print_time(threadName, delay):
    counter = 5
    while counter:
        if exitFlag:
            threadName.exit()
        time.sleep(delay)
        print ("%s: %s" % (threadName, time.ctime(time.time())))
        counter -= 1

class myMonitor (threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
    def run(self):
        print ("Starting " + self.name)
        read_sensors(self.name)
        print ("Exiting " + self.name)

def read_sensors(threadName):
    while True:
        if exitFlag:
            threadName.exit()
        time.sleep(60) # read the sensors every 60 seconds
        print ("Reading %s: %s" % (threadName, time.ctime(time.time())))
        # YOU MUST COMPLETE THE CODE TO READ THE SENSORS
        # switch_on_led(PIN_LED1)
        # global temperature
        # if temperature > 28:
            # TO COMPLETE ...
            

# Create new threads
thread1 = myThread(1, "Thread-1", 5)
thread2 = myThread(2, "Thread-2", 10)
thread3 = myMonitor(3, "Thread-Monitor")

# Start new Threads
thread1.start()
thread2.start()
thread3.start()
thread1.join()
thread2.join()
thread3.join()
print ("Exiting Main Thread")
