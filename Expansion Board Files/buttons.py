#!/usr/bin/python3
import RPi.GPIO as GPIO
import time

PIN_BUTTON1=13
PIN_BUTTON2=26

GPIO.setmode(GPIO.BCM)

GPIO.setup(PIN_BUTTON1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(PIN_BUTTON2, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def getStatesButtons():
    stateButton1 = GPIO.input(PIN_BUTTON1)
    if stateButton1 == False:
        print('Button 1 Pressed')
    else:
        print('Button 1 Not Pressed')
    stateButton2 = GPIO.input(PIN_BUTTON2)
    if stateButton2 == False:
        print('Button 2 Pressed')
    else:
        print('Button 2 Not Pressed')

# TEST
# getStatesButtons()
