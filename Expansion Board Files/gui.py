#!/usr/bin/python3
from tkinter import *
from tkinter import messagebox
import RPi.GPIO as GPIO
import time
import os
import glob
import time

#Sonda de temperatura
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
base_dir = '/sys/bus/w1/devices/'
temp_id = '28-0000079fbb30' #Poner el ID de la sonda correspondiente

#Inicializacion Raspberry
GPIO.setmode(GPIO.BCM)

Pin_button1=13
Pin_button2=26
Pin_led1=17
Pin_led2=27
Pin_led3=22
Pin_reed=5
Pin_temp=4

GPIO.setup(Pin_button1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(Pin_button2, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(Pin_reed, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(Pin_led1,GPIO.OUT)
GPIO.setup(Pin_led2,GPIO.OUT)
GPIO.setup(Pin_led3,GPIO.OUT)

#Temp sensor
try:
    device_folder = glob.glob(base_dir + temp_id)[0]
    device_file = device_folder + '/w1_slave'
except (Exception):
    print ("Error al cargar sonda temp ext")

# GUI Inicializacion

top = Tk()
top.geometry("500x350")
top.title("CyP Control Panel")

led1_val = IntVar()
led1_val.set(0)
led2_val = IntVar()
led2_val.set(0)
led3_val = IntVar()
led3_val.set(0)

led1_color="green"
led2_color="yellow"
led3_color="red"

# GUI Funciones

def helloCallBack():
   msg = messagebox.showinfo( "Hello Python", "Hello World")
def say_hi(txt):
    print("Hi: {}".format(txt))
def update_temp():
    #Tempbox2.insert(INSERT,"23")
    Func_Temp(read_temp())
def Func_Set_LED(val,pin):
    if val.get()==0:
        val.set(1)
        GPIO.output(pin, True)
    else:
        val.set(0)
        GPIO.output(pin, False)
def Func_Update_LED(dis,fig,color,pin):
    if GPIO.input(pin)==1:
        dis.itemconfig(fig,fill=color)
    else:
        dis.itemconfig(fig,fill="grey")
def Func_Button1():
    input_state = GPIO.input(Pin_button1)
    if input_state == True:
        PiButton1_val.set("NOT PRESSED")
    else:
        PiButton1_val.set("PRESSED")
def Func_Button2():
    input_state = GPIO.input(Pin_button2)
    if input_state == True:
        PiButton2_val.set("NOT PRESSED")
    else:
        PiButton2_val.set("PRESSED")
def Func_Reed():
    input_state = GPIO.input(Pin_reed)
    if input_state == False:
        reed_val.set("CLOSED")
    else:
        reed_val.set("OPEN")
def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines
def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c
    else:
        print ("Error reading temp")
def Func_Temp(val):
    temp_val.set(str(val)+"-C")
def task():
    Func_Button1()
    Func_Button2()
    Func_Reed()
    Func_Update_LED(LED1_display,oval1,led1_color,Pin_led1)
    Func_Update_LED(LED2_display,oval2,led2_color,Pin_led2)
    Func_Update_LED(LED3_display,oval3,led3_color,Pin_led3)
    
    top.after(500, task)  # reschedule event in 500 milliseconds


#LEDS

LED1_display = Canvas(top, width=25, height=25)
oval1 = LED1_display.create_oval(5,5,22,22,fill='grey')
LED1_display.place(x = 70,y = 20)

Button_led1 = Button(top, text = "LED 1", command = lambda: Func_Set_LED(led1_val,Pin_led1))
Button_led1.place(x = 50,y = 50)

LED2_display = Canvas(top, width=25, height=25)
oval2 = LED2_display.create_oval(5,5,22,22,fill='grey')
LED2_display.place(x = 170,y = 20)

Button_led2 = Button(top, text = "LED 2", command = lambda: Func_Set_LED(led2_val,Pin_led2))
Button_led2.place(x = 150,y = 50)

LED3_display = Canvas(top, width=25, height=25)
oval3 = LED3_display.create_oval(5,5,22,22,fill='grey')
LED3_display.place(x = 270,y = 20)

Button_led3 = Button(top, text = "LED 3", command = lambda: Func_Set_LED(led3_val,Pin_led3))
Button_led3.place(x = 250,y = 50)

#TEMPERATURE

Button_T1 = Button(top, text = "Update Temp", command = update_temp)
Button_T1.place(x = 250,y = 105)

temp_val = StringVar()
Tempbox1 = Label( top, text = "TEMPERATURE: ", relief = FLAT )
Tempbox1.place(x = 50,y = 110)
Tempbox2 = Label( top, textvariable = temp_val, relief = RIDGE )
Tempbox2.place(x = 170,y = 110)
temp_val.set("19-C")

#REED Sensor
reed_val = StringVar()
Reedbox = Label( top, text = "REED SENSOR: ", relief = FLAT )
Reedbox.place(x = 50,y = 160)
Reedbox2 = Label( top, textvariable = reed_val, relief = RIDGE )
Reedbox2.place(x = 170,y = 160)
reed_val.set("OPEN")

#BUTTON1
PiButton1_val = StringVar()
PiButton1 = Label( top, text = "BUTTON 1: ", relief = FLAT )
PiButton1.place(x = 50,y = 210)
PiButton1b = Label( top, textvariable = PiButton1_val, relief = RIDGE )
PiButton1b.place(x = 170,y = 210)
PiButton1_val.set("PRESSED")

#BUTTON2
PiButton2_val = StringVar()
PiButton2 = Label( top, text = "BUTTON 2: ", relief = FLAT )
PiButton2.place(x = 50,y = 260)
PiButton2b = Label( top, textvariable = PiButton2_val, relief = RIDGE )
PiButton2b.place(x = 170,y = 260)
PiButton2_val.set("PRESSED")

#QUIT
Button_quit = Button(top, text="QUIT", fg="red",command=top.destroy)
Button_quit.pack(side="bottom")

# Tasks
top.after(1000, task)
top.mainloop()
